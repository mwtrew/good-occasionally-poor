extends Area2D

var damage = 2
var speed = 20
var is_night

func _ready():
	if is_night:
		$AnimatedSprite.play("night-default")
	else:
		$AnimatedSprite.play('default')
	var _area_conn = self.connect("area_entered", self, "_on_Area2D_area_entered")

func _process(delta):
	if self.position.x < 0:
		self.queue_free()

	self.position.x -= speed * delta

func _on_Area2D_area_entered(area):
	if area.is_in_group("player"):
		area.damage(damage, self)

