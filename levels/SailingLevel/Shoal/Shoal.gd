extends Node2D


# I can only apologise to the reader of the following
var x = 0
# fishy wiggle speed, in cycles per something or other
var wiggle_vigour = 0.6

const y_range_start = 80
const y_range_end = 192
var y_range = y_range_end - y_range_start
var y_range_midpoint = y_range_start + y_range / 2
var y_range_modifier = 0.5

# how long does the shoal hang about for?
var time_to_live = 60
var type = "halibut"
var is_night 

signal fish_caught


func _ready():		
	var _ttl_conn = $Timer.connect('timeout', self, '_on_Timer_timeout')
	$Timer.start(time_to_live)

	for fish in get_children():
		var _c = fish.connect("caught", self, "_on_Fish_caught")

	self.set_night(self.is_night)

func _process(delta):
	self.position.y = y_range_midpoint + (y_range_modifier * (sin(x) * (y_range / 2)))
	x += wiggle_vigour * delta

func set_night(value):
	if value:
		for fish in get_children():
			if fish.is_in_group('fishies'):
				fish.is_night = true
				fish.play_animation('default')

func _on_Fish_caught():
	emit_signal("fish_caught", self.type)

func _on_Timer_timeout():
	print("%s: it is time for us to go" % self.name)
	self.queue_free()



