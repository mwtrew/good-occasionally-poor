extends Area2D


var speed = 20
var boundary
var is_night


func _process(delta):
	if Input.is_action_pressed("ui_right"):
		$AnimatedSprite.flip_h = true
		self.position.x += speed * delta

	if Input.is_action_pressed("ui_left"):
		$AnimatedSprite.flip_h = false
		self.position.x -= speed * delta

	# bind position to the size of the boat (hard-coding not ideal)
	self.position.x = min(self.position.x, 20)
	self.position.x = max(self.position.x, -20)

func set_night(value):
	if value:
		is_night = true
		self.play_animation($AnimatedSprite.animation)

func play_animation(animation):

	animation = animation.replace("night-", "")
	

	if self.is_night:
		$AnimatedSprite.play("night-" + animation)
	else:
		$AnimatedSprite.play(animation)