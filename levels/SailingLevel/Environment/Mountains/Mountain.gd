extends Node2D

var is_night
var speed = 1
var mountains = [
	"res://levels/SailingLevel/Environment/Mountains/hill-1.png",
	"res://levels/SailingLevel/Environment/Mountains/hill2.png",
	"res://levels/SailingLevel/Environment/Mountains/hill-3.png",
	"res://levels/SailingLevel/Environment/Mountains/hill-4.png"
]

var night_mountains = [
	'sprites/night-sailing/night-hill-1.png',
	'sprites/night-sailing/night-hill2.png',
	'sprites/night-sailing/night-hill-3.png',
	'sprites/night-sailing/night-hill-4.png'
]

var image_index

func _ready():
	var mountain
	image_index = randi() % len(self.mountains)
	if is_night:
		mountain = self.night_mountains[image_index]
	else:
		mountain = self.mountains[image_index]
	load_image(mountain)


func _process(delta):
	self.position.x -= speed * delta

func set_night(value):
	self.is_night = value
	if self.is_night:
		var mountain = self.night_mountains[self.image_index]
		self.load_image(mountain)

func load_image(mountain):
	var image = Image.new()
	image.load(mountain)
	$TextureRect.texture = ImageTexture.new()
	$TextureRect.texture.create_from_image(image, 0)
