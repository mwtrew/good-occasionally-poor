extends AnimatedSprite

var is_night
var speed = 3
var squawks = [
	"res://levels/SailingLevel/Environment/Seagull/GSP-gull-solo-1.tres",
	"res://levels/SailingLevel/Environment/Seagull/GSP-gull-solo-2.tres",
	"res://levels/SailingLevel/Environment/Seagull/GSP-gull-solo-3.tres",
	"res://levels/SailingLevel/Environment/Seagull/GSP-gull-solo-4.tres",
	"res://levels/SailingLevel/Environment/Seagull/GSP-gull-solo-5.tres",
	"res://levels/SailingLevel/Environment/Seagull/GSP-gull-solo-6.tres"
]


func _ready():
	var _c = $SquawkTimer.connect("timeout", self, "_on_SquawkTimer_timeout")
	
	# choose squawk sound at random
	var sample = self.squawks[randi() % len(self.squawks)]
	$AudioStreamPlayer2D.stream = load(sample)
	
	# choose animation at random
	var animations = ['far', 'medium']
	
	self.play_animation(animations[randi() % animations.size()])
		
	# squawk upon spawning
	$AudioStreamPlayer2D.play()

func _process(delta):
	if self.position.x > ProjectSettings.get_setting("display/window/size/width"):
		self.queue_free()

	self.position.x += self.speed * delta

func set_night(value):
	if value:
		self.is_night = true
		self.play_animation(self.animation)

func play_animation(animation):

	animation = animation.replace("night-", "")
	

	if self.is_night:
		self.play("night-" + animation)
	else:
		self.play(animation)
	

func _on_SquawkTimer_timeout():
	$AudioStreamPlayer2D.play()
	$SquawkTimer.start(randi() % 80)
