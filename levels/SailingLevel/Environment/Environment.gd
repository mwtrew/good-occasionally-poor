extends Node2D


var resolution = Vector2(ProjectSettings.get_setting("display/window/size/width"), ProjectSettings.get_setting("display/window/size/height"))
const sea_extent_y = 80

var spawn_gulls = true
var spawn_mountains = true
var is_night


func _ready():
	var _mountain_spawn_conn = $MountainSpawnTimer.connect("timeout", self, "_on_MountainSpawnTimer_timeout")
	var _gull_spawn_conn = $GullSpawnTimer.connect("timeout", self, "_on_GullSpawnTimer_timeout")

func spawn_mountain():
	if self.spawn_mountains:
		var spawn_location = Vector2(self.resolution.x, self.sea_extent_y)
		print("%s: spawning a mountain at %s" % [self.name, str(spawn_location)])
		var mountain_scene = preload("res://levels/SailingLevel/Environment/Mountains/Mountain.tscn").instance()
		mountain_scene.set_position(spawn_location)
		self.add_child(mountain_scene)
		mountain_scene.set_night(is_night)
		mountain_scene.add_to_group("mountains")
	else:
		print("%s: did not spawn mountain - spawning disabled" % [self.name])

func spawn_gull(spawn_location):
	if self.spawn_gulls:
		print("%s: spawning a gull at %s" % [self.name, str(spawn_location)])
		var gull_scene = preload("res://levels/SailingLevel/Environment/Seagull/Seagull.tscn").instance()
		gull_scene.set_position(spawn_location)
		self.add_child(gull_scene)
		gull_scene.set_night(self.is_night)
		gull_scene.add_to_group("gulls")
	else:
		print("%s: did not spawn gull - spawning disabled" % [self.name])

func enable_gull_spawning(value):
	self.spawn_gulls = value
	print("%s: spawn gulls set to %s" % [self.name, str(self.spawn_gulls)])

func enable_mountain_spawning(value):
	self.spawn_mountains = value
	print("%s: spawn mountains set to %s" % [self.name, str(self.spawn_mountains)])

func start_weather():
	$Weather.start()

func stop_weather():
	$Weather.stop()

func set_night(value):
	self.is_night = value
	for child in get_children():
		if child.is_in_group("mountains") or child.is_in_group("gulls"):
			child.set_night(value)

func _on_MountainSpawnTimer_timeout():
	self.spawn_mountain()
	
func _on_GullSpawnTimer_timeout():
	self.spawn_gull(Vector2(0, randi() % self.sea_extent_y))
	$GullSpawnTimer.start(randi() % 120)
	
