extends Area2D


signal caught
var is_night

func _ready():
	self.is_night = get_parent().is_night
	self.play_animation('default')
		
	self.add_to_group("fish")
	var _c = self.connect("area_entered", self, "_on_Area2D_area_entered")

func play_animation(animation):

	animation = animation.replace("night-", "")
	
	if self.is_night:
		$AnimatedSprite.play("night-" + animation)
	else:
		$AnimatedSprite.play(animation)

func _on_Area2D_area_entered(area):
	if area.is_in_group('line'):
		emit_signal("caught")
		self.queue_free()
