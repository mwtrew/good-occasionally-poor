extends Area2D


# movement speed (lateral) in pixels/s
var speed = 15
var health = 2
var bobbing = false
var dead = false
var invulnerable = false
var can_throw_hook = true
var hook_scene = preload('res://levels/SailingLevel/Boat/Hook/Hook.tscn')

signal destroyed

var is_night 
var time
var set_docked
var back_home


func _ready():
	var _bob_conn = $BobTimer.connect("timeout", self, "_on_BobTimer_timeout")
	var _crash_conn = $CrashTimer.connect("timeout", self, "_on_CrashTimer_timeout")
	var _crash_cool_conn = $CrashCooldownTimer.connect("timeout", self, "_on_CrashCooldownTimer_timeout")
	var _hook_cool_conn = $HookCooldownTimer.connect("timeout", self, "_on_HookCooldownTimer_timeout")
	var _hook_throw_cool_conn = $HookThrownCooldownTimer.connect("timeout", self, "_on_HookThrownCooldownTimer_timeout")

func _process(delta):
	if back_home:
		self.set_night(true)
		is_night = true
		$Fisherman.set_night(true)
		$AnimatedSprite.flip_h = true
		self.position.x -= 10 * delta

	# todo: this is getting messy, refactor to FSM or something
	if !dead && !set_docked:
		var fisherman_at_helm = self.is_fisherman_at_helm()

		if Input.is_action_pressed("ui_up") && fisherman_at_helm:
			self.position.y -= speed * delta

		if Input.is_action_pressed("ui_down") && fisherman_at_helm:
			self.position.y += speed * delta

		# todo: keep the boat in the sea in a less bad way
		self.position.y = min(self.position.y, 192)
		self.position.y = max(self.position.y, 90)

		if Input.is_action_just_pressed("input_f") && !fisherman_at_helm && can_throw_hook:
			self.cast_hook()

	# # suicide button for debug purposes
	# if Input.is_action_just_pressed("ui_cancel"):
	# 	self.die()

func damage(value, _source):
	if !dead && !invulnerable:
		$CrashTimer.start()
		$CrashCooldownTimer.start()
		self.play_animation("crash")
		$Fisherman.play_animation("crash")
		$CrashAudioStreamPlayer2D.play()
		self.health -= value
		self.invulnerable = false
		print("%s: sustained %s damage, health: %s" % [self.name, value, self.health])
		if health <= 0:
			die()

func die():
	emit_signal("destroyed")
	health = 2

func is_fisherman_at_helm():
	return $Fisherman in $CabinArea2D.get_overlapping_areas()

func cast_hook():
	$HookThrownCooldownTimer.start()
	var hook = self.hook_scene.instance()
	hook.set_position($Fisherman.global_position)
	hook.add_to_group("line")
	get_parent().add_child(hook)
	can_throw_hook = false
	$HookCooldownTimer.start()
	$CastGruntHookAudioStreamPlayer.play()

func set_night(value):
	if value:
		is_night = true
		self.play_animation($AnimatedSprite.animation)
		$Fisherman.set_night(true)

func set_docked(value):
	if value:
		$Fisherman.play_animation('default')
		self.play_animation("still")
		self.set_docked = true

func play_animation(animation):
	animation = animation.replace("night-", "")
	
	if self.is_night:
		$AnimatedSprite.play("night-" + animation)
	else:
		$AnimatedSprite.play(animation)

func _on_BobTimer_timeout():
	if self.bobbing:
		self.position.y += 1
		self.bobbing = false
	else: 
		self.position.y -= 1
		self.bobbing = true

func _on_CrashTimer_timeout():
	self.play_animation('default')
	$Fisherman.play_animation('default')

func _on_CrashCooldownTimer_timeout():
	self.invulnerable = false

func _on_HookCooldownTimer_timeout():
	self.can_throw_hook = true
	$Fisherman.play_animation('default')
	
func _on_HookThrownCooldownTimer_timeout():
	$Fisherman.play_animation('reeling')
	$ReelingInAudioStreamPlayer.play()
	
