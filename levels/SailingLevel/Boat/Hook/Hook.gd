extends Area2D


var _gravity = 4
var motion = Vector2(0, 0)
var spawn_position = Vector2()
var tolerance = 10

func _ready():
	var _hook_conn = $HookSplashTimer.connect("timeout", self, "_on_HookSplashTimer_timeout")
	var _hook_splash = self.connect("animation_finished", self, "_on_sunset_animation_finished")

	self.spawn_position = self.position
	$HookAnimatedSprite.play()

	# apply some upward & sideward force for the cast
	motion += Vector2(90, -90)
	var _c = self.connect("area_entered", self, "_on_Area2D_area_entered")

func _process(delta):
	get_parent().get_node("HookLine2D").add_point(self.position)

	if self.position.y > self.spawn_position.y + tolerance:
		self._despawn()

	# apply gravitational force
	motion.y += _gravity
	self.position += motion * delta

func _despawn():
	get_parent().get_node("HookLine2D").clear_points()
	# $HookAnimatedSprite.play("splash")
	# self.position = Vector2(130.076263, 139.807144)
	# yield($HookAnimatedSprite, "animation_finished")
	self.queue_free()


func _on_Area2D_area_entered(area):
	if area.is_in_group("fish"):
		self._despawn()
			

	
