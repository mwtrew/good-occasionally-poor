extends Node2D
var is_night = true
signal show_game_over_ui


func _ready():
	$Boat.set_night(is_night)
	$Boat.is_night = is_night
	$Boat.back_home = true

	var _c = $ShowGameOverUITimer.connect('timeout', self, '_on_ShowGameOverUITimer_timeout')

func start(_params):
	$ShowGameOverUITimer.start()

func stop():
	pass
	
func connect_level_signals(game):
	var _c = self.connect("show_game_over_ui", game, "_on_Level_show_score")

func _on_ShowGameOverUITimer_timeout():
	emit_signal("show_game_over_ui")
