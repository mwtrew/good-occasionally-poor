extends Node2D


# the group that is to move up, the other group moves down
var bobbing_group = "ambient_boats_a"


func _ready():
	# var _conn = $BobTimer.connect("timeout", self, "_on_BobTimer_timeout")
	pass

func _on_BobTimer_timeout():
	for boat in self.get_children():
		if boat.is_in_group(bobbing_group):
			boat.position.y -= 1
		elif boat.is_in_group(self.get_other_bobbing_group(self.bobbing_group)):
			boat.position.y += 1
		
	self.bobbing_group = self.get_other_bobbing_group(self.bobbing_group)

func get_other_bobbing_group(_bobbing_group):
	if _bobbing_group == "ambient_boats_a":
		return "ambient_boats_b"

	if _bobbing_group == "ambient_boats_b":
		return "ambient_boats_a"
