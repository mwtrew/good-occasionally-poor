extends "res://logic/SailingEvent/AbstractSailingEvent.gd"


const rock_scene = "res://levels/SailingLevel/Rock/Rock.tscn"
var rock_interval_duration = 1
var rock_interval_timer = Timer.new()


func begin_event():
	print("%s: doing my thing (evil rocks)" % self.name)
	rock_interval_timer.one_shot = false
	rock_interval_timer.connect("timeout", self, "_on_RockIntervalTimer_timeout")
	self.add_child(rock_interval_timer)
	rock_interval_timer.start(self.rock_interval_duration)

func end_event():
	rock_interval_timer.disconnect("timeout", self, "_on_RockIntervalTimer_timeout")
	.end_event()

func spawn_rock():
	var spawn_location = Vector2(self.level.resolution.x, self.level.sea_extent_y + randi() % (int(self.level.resolution.y) - self.level.sea_extent_y))
	self.level.spawn_scene_at_position(preload(rock_scene).instance(), spawn_location)

func _on_RockIntervalTimer_timeout():
	self.spawn_rock()
