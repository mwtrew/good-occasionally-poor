extends "res://logic/SailingEvent/AbstractSailingEvent.gd"


const seaweed_scene = "res://levels/SailingLevel/Seaweed/Seaweed.tscn"
var seaweed_interval_duration = 2
var seaweed_interval_timer = Timer.new()


func begin_event():
	print("%s: doing my thing (evil seaweeds)" % self.name)
	seaweed_interval_timer.one_shot = false
	seaweed_interval_timer.connect("timeout", self, "_on_SeaweedIntervalTimer_timeout")
	self.add_child(seaweed_interval_timer)
	seaweed_interval_timer.start(self.seaweed_interval_duration)

func end_event():
	seaweed_interval_timer.disconnect("timeout", self, "_on_SeaweedIntervalTimer_timeout")
	.end_event()

func spawn_seaweed():
	var spawn_location = Vector2(self.level.resolution.x, self.level.sea_extent_y + randi() % (int(self.level.resolution.y) - self.level.sea_extent_y))
	self.level.spawn_scene_at_position(preload(seaweed_scene).instance(), spawn_location)

func _on_SeaweedIntervalTimer_timeout():
	self.spawn_seaweed()
