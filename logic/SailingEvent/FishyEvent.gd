extends "res://logic/SailingEvent/AbstractSailingEvent.gd"


var type = "halibut"
var surface_time = 45


func begin_event():
	print("%s: the fishies have made themselves known to you." % self.name)
	self.spawn_shoal(type, surface_time)

func spawn_shoal(fish_type, fish_surface_time):
	var shoal_instance = preload("res://levels/SailingLevel/Shoal/Shoal.tscn").instance()
	shoal_instance.type = fish_type
	shoal_instance.time_to_live = fish_surface_time
	self.level.spawn_scene_at_position(shoal_instance, Vector2(128, 0), "Shoal")
	var _c = shoal_instance.connect("fish_caught", self.level, "_on_Shoal_fish_caught")
