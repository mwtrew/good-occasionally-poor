extends Node


var fish_values = {
	"bluefish": 2,
	"greenfish": 5,
	"nightfish": 10,
	"grimfish": 20
}

var fish_catches = {
	"bluefish": 0,
	"greenfish": 0,
	"nightfish": 0,
	"grimfish": 0
}

var total_money = 0


func catch_fish(type):
	self.fish_catches[type] += 1

func count_catches_of_type(type):
	return self.fish_catches[type]

func evaluate_haul(exceeded_time_limit):
	var value = 0

	if !exceeded_time_limit:
		for fish_type in fish_values.keys():
			value += self.evaluate_catches_of_type(fish_type)
	return value

func evaluate_catches_of_type(type):
	return self.count_catches_of_type(type) * fish_values[type]

func clear_catches():
	for type in fish_catches.keys():
		fish_catches[type] = 0

