extends Node


signal time_updated


var economy = null
var route_data = null

var current_level_id
var level_scenes = {
	"sailing": 'res://levels/SailingLevel/SailingLevel.tscn',
	"port": 'res://levels/PortLevel/PortLevel.tscn',
	"night-port": 'res://levels/NightPortLevel/NightPortLevel.tscn'
}

var ui_scenes = {
	"default": 'res://ui/AbstractUI/AbstractUI.tscn',
	"game_over": 'res://ui/GameOverUI/GameOverUI.tscn',
	"book": 'res://ui/FishLexiconUI/FishLexiconScene.tscn',
	"map": 'res://ui/MapUI/MapScene.tscn',
	"list": 'res://ui/FishListUI/FishListScene.tscn',
	"sailing": 'res://ui/SailingUI/SailingUI.tscn'
}

var day = 1
var time_allowed = 12
var time_taken = 0

var skip_intro = false


func _ready():
	self.economy = $Economy

	self.set_ui('default')
	self.set_level("port")

	self.fade_in()

func clear_level():
	remove_child($Level)

func set_level(level_id):
	print("%s: set_level called - %s" % [self.name, level_id])
	$Level.free()
	var level_instance = load(self.level_scenes[level_id]).instance()
	level_instance.set_name("Level")
	level_instance.is_night = false
	add_child(level_instance)
	self.current_level_id = level_id
	level_instance.connect_level_signals(self)
	level_instance.start(self)

func get_level_id():
	return self.current_level_id

func set_ui(ui_id):
	print("%s: set_ui called - %s" % [self.name, ui_id])
	var ui_instance = load(self.ui_scenes[ui_id]).instance()
	ui_instance.set_name("UI")
	remove_child($UI)
	add_child(ui_instance)
	ui_instance.connect_ui_signals(self)
	ui_instance.start(self)

func fade_in():
	$CanvasModulate/AnimationPlayer.play('fade_in')

func fade_out():
	$CanvasModulate/AnimationPlayer.play('fade_out')

func exceeded_time_limit():
	return self.time_taken > self.time_allowed

func _on_Level_boat_destroyed():
	$UI.show_dialog("sailing", "crashed")

func _on_Level_show_dialog(conversation_key, line_key):
	$UI.show_dialog(conversation_key, line_key)

func _on_UI_change_level_command(level_id, ui_id):
	self.set_ui(ui_id)
	call_deferred("set_level", level_id)

func _on_UI_exit_to_menu_command():
	self.set_ui("main_menu")
	self.clear_level()

func _on_UI_exit_game_command():
	get_tree().notification(MainLoop.NOTIFICATION_WM_QUIT_REQUEST)

func _on_UI_restart_command():
	self.day += 1
	self.economy.total_money += self.economy.evaluate_haul(self.exceeded_time_limit())
	self.economy.clear_catches()

	self.skip_intro = true
	self.set_ui('default')
	self.fade_out()
	yield($CanvasModulate/AnimationPlayer, "animation_finished")
	self.set_level("port")
	self.fade_in()

func _on_Level_show_book():
	self.set_ui("book")

func _on_Level_show_list():
	self.set_ui("list")
	
func _on_Level_show_map():
	self.set_ui("map")

func _on_Level_show_score():
	self.set_ui("game_over")

func _on_UI_exit_menu_command():
	self.set_ui("default")

func _on_route_generated(route_head):
	print("%s: route generated called - %s" % [self.name, route_head])
	self.route_data = route_head

func _on_Level_tutorial_finished():
	$MusicPlayer.play_now("theme")

func _on_Level_fish_caught(type):
	print("%s: a %s was caught" % [self.name, type])
	$Economy.catch_fish(type)

func _on_Level_change_level(level_id, ui_id):
	call_deferred("set_level", level_id)
	self.set_ui(ui_id)

func _on_Level_time_elapsed(time):
	self.time_taken = time
	emit_signal("time_updated", self.time_allowed - self.time_taken)

func _on_Level_night_time_reached():
	randomize()
	var song = $MusicPlayer.categories['spooky'][randi() % len($MusicPlayer.categories['spooky'])]
	$MusicPlayer.play_now(song)
