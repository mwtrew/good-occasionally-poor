# Good, Occasionally Poor
A hebridean fisherman battles the elements to catch tasty fish.

![I6P8u8.png](images/I6P8u8.png)
![IRAvvw.png](images/IRAvvw.png)
![g1Lj+E.png](images/g1Lj+E.png)

Created for the 2-week [ZEN JAM 03.](https://longdog.itch.io/good-occasionally-poor)

## Credits
A game by Jack McAuley (audio), Ottilia Westerlund (art and programming), and Matthew Trew (programming)

### Additional credits for library material:
* Seagull - beejeeb1314 ([CC attribution](https://creativecommons.org/licenses/by/3.0/))
* Seaport Ambience Vlissingen - klankbeeld ([CC attribution](https://creativecommons.org/licenses/by/3.0/))
* Rain on Roof - The Boatman ([CC attribution](https://creativecommons.org/licenses/by/3.0/))
* Lake Light Rain - Eugene7_eSwai ([CC non-commercial](https://creativecommons.org/licenses/by-nc/4.0/))
* UrbanHerringGulls - acclivity ([CC non-commercial](https://creativecommons.org/licenses/by-nc/4.0/))
* shipcreaks2 - deleted_user_13 (public domain)
* Moored Boat Metal Grinding - Jagadamba ([CC attribution](https://creativecommons.org/licenses/by/3.0/))
* Waves Crashing Against Wall - Ali_6868 (public domain)
* Splash - Tran5ient (public domain)
* Qunique Five font - ggbot ([SIL OFL](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL))
* Alagard font - Hewett Tsoi (public domain)

