extends AudioStreamPlayer


var tracks = {
	'theme': "res://music/GSP-Melanchol-Theme.wav",
	'selkie_waltz': "res://music/forlorn_selkie_waltz.ogg",
	'frontier_tune': "res://music/frontier_tune.ogg",
	'blue_man_challenge': "res://music/blue_mans_challenge.ogg"
}

var queue = []
var categories = {
	"spooky": ["selkie_waltz", "blue_man_challenge"],
	"not_spooky": ["frontier_tune", "theme"]
}

func play_now(track_id):
	self.stop()
	self.stream = load(tracks[track_id])
	self.play()

func fade_out():
	$AnimationPlayer.play('fade_out')
