extends CanvasLayer


signal exit_menu


func start(game):
	pass

func _process(_delta):
	if Input.is_action_just_pressed("ui_cancel"):
		emit_signal("exit_menu")

func connect_ui_signals(game):
	var _c = self.connect("exit_menu", game, "_on_UI_exit_menu_command") 

func show_dialog(conversation, key):
	print("%s: show_dialog called - %s, %s" % [self.name, conversation, key])
	var dialog_node_scene = preload('res://ui/DialogUI/Dialog.tscn')
	var lines = preload('res://ui/DialogUI/Conversations.gd').new().conversations[conversation][key]

	var dialog_instance = dialog_node_scene.instance()
	dialog_instance.dialog = lines
	dialog_instance.name = "Dialog"
	self.add_child(dialog_instance)


