extends RichTextLabel


var transparent_colour = Color(1.0, 1.0, 1.0, 0.0)


func _ready():
	self.modulate = transparent_colour

func fade_in():
	$AnimationPlayer.play('fade_in')

func fade_out():
	$AnimationPlayer.play('fade_out')
