extends "res://ui/AbstractUI/AbstractUI.gd"


var route_head
var estimated_time_cost = 0
var fish_types
signal start_sailing
signal route_generated

func _process(_delta):
	if Input.is_action_just_pressed("ui_accept"):
		emit_signal("start_sailing", "sailing", "sailing")
	
func start(game):
	self.fish_types = game.economy.fish_values.keys()

	route_head = self.generate_route()
	self.plot_path(route_head)
	self.get_voyage_time(route_head)
	$EstimatedTimeRichTextLabel.bbcode_text="[right]estimated: %sh[/right]" % self.estimated_time_cost
	emit_signal("route_generated", route_head)

# hard-coded (for now...)
func generate_route():
	randomize()

	$Waypoints/MapWaypoint12.next_waypoint = $Waypoints/MapWaypoint
	$Waypoints/MapWaypoint12.add_event("null", 30) # 30
	$Waypoints/MapWaypoint12.time_cost = 0

	$Waypoints/MapWaypoint.next_waypoint = $Waypoints/MapWaypoint3
	$Waypoints/MapWaypoint.add_event("rocks", 30) # 45
	$Waypoints/MapWaypoint.add_event("fish", 5, {'type': fish_types[randi() % fish_types.size()], 'surface_time': 45})
	$Waypoints/MapWaypoint.add_event("rocks", 15)
	$Waypoints/MapWaypoint.time_cost = 1.5
	
	$Waypoints/MapWaypoint3.next_waypoint = $Waypoints/MapWaypoint6
	$Waypoints/MapWaypoint3.add_event("null", 15)
	$Waypoints/MapWaypoint3.add_event("seaweed", 25)
	$Waypoints/MapWaypoint3.time_cost = 1.5

	$Waypoints/MapWaypoint6.next_waypoint = $Waypoints/MapWaypoint8
	$Waypoints/MapWaypoint6.add_event("null", 10)
	$Waypoints/MapWaypoint6.add_event("weather_start", 5)
	$Waypoints/MapWaypoint6.add_event("ship", 20)
	$Waypoints/MapWaypoint6.time_cost = 1.5

	$Waypoints/MapWaypoint8.next_waypoint = $Waypoints/MapWaypoint7
	$Waypoints/MapWaypoint8.add_event("null", 15)
	$Waypoints/MapWaypoint8.add_event("fish", 5, {'type': fish_types[randi() % fish_types.size()], 'surface_time': 45})
	$Waypoints/MapWaypoint8.add_event("rocks", 20)
	$Waypoints/MapWaypoint8.time_cost = 1.5

	$Waypoints/MapWaypoint7.next_waypoint = $Waypoints/MapWaypoint9
	$Waypoints/MapWaypoint7.add_event("weather_stop", 5)
	$Waypoints/MapWaypoint7.add_event("return_to_port", 5)
	$Waypoints/MapWaypoint7.time_cost = 1.5

	return $Waypoints/MapWaypoint12

func plot_path(_route_head):
	var waypoint = _route_head
	while waypoint != null:
		$Route.add_point(waypoint.position)
		waypoint = waypoint.next_waypoint

func get_voyage_time(_route_head):
	var waypoint = _route_head
	while waypoint != null:
		self.estimated_time_cost += waypoint.time_cost
		waypoint = waypoint.next_waypoint
		
func connect_ui_signals(game):
	.connect_ui_signals(game)
	var _sail_c = self.connect("start_sailing", game, "_on_UI_change_level_command")
	var _route_c = self.connect("route_generated", game, "_on_route_generated")
