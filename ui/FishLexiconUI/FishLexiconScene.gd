extends "res://ui/AbstractUI/AbstractUI.gd"

var pages = []
var page_index = 1

func _ready():
	$Page1.visible = true
	$Page2.visible = false

func _process(_delta):
	#This won't work with more pages so needs redone later.

	if Input.is_action_just_pressed("ui_accept"):
		$Page2.visible = !$Page2.visible
		$Page1.visible = !$Page1.visible
		
	# $NextIndicator.visible = finished

	# var desired_children = get_tree().get_nodes_in_group("pages")
	# print(desired_children)
	# if page_index < pages.size():
	# 	print(page_index)
	# 	finished = false
	# 	$Page[page_index].visible = true
