extends Node


var conversations = {
	"port_tutorial": {
		"move": [
			"*Radio noise* Hello my old friend! How are ye?",
			"(Use the [color=yellow]arrow keys[/color] to move around the boat)"
		],
		"order": [
			"Special orders today. Keep an eye out for these fellows.",
			"(Press [color=yellow]C[/color] to view your [color=yellow]fishing list[/color])",
		],
		"bestiary": [
			"(Press [color=yellow]X[/color] to view the [color=yellow]fish book[/color] to learn about the fish.)"
		],
		"objective": [
			"There's some horrible rocks out there, amongst other things. Watch yersel.",
			"you'll need to be back in [color=yellow]12 hours[/color] or you'll miss the market again."
		],
		"map": [
			"Here's your route. I tried especially to keep you away from the rocks this time.",
			"(Press [color=yellow]Z[/color] to view the [color=yellow]map[/color] and begin your voyage)"
		]
	},
	"sailing": {
		"crashed": [
			"Your vessel was damaged by the collision.",
			"It was [color=yellow]two hours[/color] before you could set sail once more."
		]
	},
	"fish_tutorial": {
		"fishing": [
			"Time to do some fishing!",
			"(Move between the [color=yellow]helm[/color] and the [color=yellow]decks[/color] with the [color=yellow]left[/color] and [color=yellow]right[/color] arrow keys)",
			"(Navigate from the [color=yellow]helm[/color] of the boat with the [color=yellow]up[/color] and [color=yellow]down[/color] arrow keys)",
			"(You need to be at the [color=yellow]helm[/color] to steer the boat!)",
			"(Press [color=yellow]F[/color] from the [color=yellow]decks[/color] to cast the [color=yellow]fishing line[/color])",
			"(You need to be on the [color=yellow]decks[/color] to cast the line!)"
		]
	}
}
